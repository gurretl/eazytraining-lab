provider "aws" {
  region     = "us-east-1"
  access_key = "YOUR OWN"
  secret_key = "YOUR OWN"
}

# Création Security Group
module "sgmod" {
  source = "../modules/securitygroup"
}

# Création instance EC2 en utilisant le module créé au préalable
# Installation de Nginx et link Security Group
module "ec2mod" {
  source          = "../modules/ec2"
  security_groups = module.sgmod.name
  sgname          = module.sgmod.sgname
  # Surcharge variables
  aws_common_tag = {
    Name = "ec2-lionel"
  }
  instancetype = "t2.micro"
}

## Création volume EBS secondaire en utilisant le module créé au préalable
module "ebsmod" {
  source = "../modules/ebs"
  # Variable surchargee
  aws_ebs_size = 10
}

## On attache le volume EBS à notre instance EC2
resource "aws_volume_attachment" "data-vol" {
  device_name = "/dev/xvdb"
  volume_id   = module.ebsmod.id
  instance_id = module.ec2mod.id
}

## Création LB et link Custom Instance
module "eipmod" {
  source             = "../modules/eip"
  custom_instance_id = module.ec2mod.id
}
