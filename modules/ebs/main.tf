# Création volume EBS
resource "aws_ebs_volume" "myebsvolume" {
  availability_zone = "us-east-1a"
  size              = var.aws_ebs_size
  tags = {
    Name = "EBS-Lionel"
  }
}

output "id" {
  value = aws_ebs_volume.myebsvolume.id
}
