# Création de l EIP
resource "aws_eip" "lb" {
  instance = var.custom_instance_id
  vpc      = true
  # Ajout de l'IP dans un fichier texte
  provisioner "local-exec" {
    command = "echo PUBLIC IP: ${aws_eip.lb.public_ip} > infos_ec2.txt"
  }
}
output "id" {
  value = aws_eip.lb.id
}
