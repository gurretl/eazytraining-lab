variable instancetype {
  type        = string
  description = "set AWS instance type"
  default     = "t2.nano"
}

variable sgname {
  type    = string
  default = "mysg"
}

variable aws_size {
  type    = number
  default = 8
}

variable aws_common_tag {
  type        = map
  description = "Set aws tag"
  default = {
    Name = "ec2-eazytraining"
  }
}
