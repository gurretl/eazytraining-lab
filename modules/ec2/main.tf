# Récupération de l image Ubuntu (j ai du m inspirer d un autre site pour ce point)
data "aws_ami" "app_ami" {
  most_recent = true
  owners      = ["099720109477"] # je n'ai pas compris comment trouver cette info
  filter {
    name = "name"
    # idem je n ai pas compris comment touver cette info
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }
}
# Création de l'instance EC2
resource "aws_instance" "myec2" {
  ami             = data.aws_ami.app_ami.id
  instance_type   = var.instancetype
  key_name        = "devops-lionel"
  tags            = var.aws_common_tag
  security_groups = [var.sgname]
  root_block_device {
    delete_on_termination = true
    volume_size           = var.aws_size
  }
  provisioner "remote-exec" {
    inline = [
      "sudo apt-get install -y nginx",
      "sudo systemctl start nginx"
    ]
    connection {
      type        = "ssh"
      user        = "ubuntu"
      private_key = file("./devops-lionel.pem")
      host        = self.public_ip
    }
  }
}
output "id" {
  value = aws_instance.myec2.id
}
