# README #

TP MiniProjet leçon Terraform les bases indispensables - Lionel Gurret

### Notes ###

* J'ai eu principalement des soucis avec l'image ubuntu. Je ne suis pas sûr que ce soit la bonne façon de faire.
* J'ai perdu du temps aussi sur l option "output" qui permet de récupérer des infos des modules mais j'ai bien appris :)
* J'ai eu aussi des erreurs bizarres que j'ai pu corriger en me connectant au VPN de mon entreprise, en supprimant le tfstate ou en recréant un accès AWS :

Error: AuthFailure: AWS was not able to validate the provided access credentials

Je n'ai pas trop compris pourquoi.

Merci pour tout Dirane je pense que j'ai beaucoup appris, je vais commencer le cours pour la certification.
